# Maintenance

## Release process

Releases are performed automatically by Gitlab when a `lisa-*` tag is
pushed (see `.gitlab-ci.yml`).  Releases are uploaded to the project
package repository in Gitlab.

These are the steps to be performed:

1. Ensure that `CHANGELOG` is up to date.  Ideally, changes worth
   mentioning there should have been added when the change was done.

   Be very careful with the format of the `CHANGELOG`.  The release
   script expects each release version to be on its own section and
   starting with a `#`, e.g., `# [0.0.3] - 2021-10-08`.  The script
   then assumes that the next line starting with `#` is the start of
   the changes for the last version.  If lines other than those start
   with `#`, the release description of each release in Gitlab will be
   wrong.

2. Specify the new version, commit, and tag it:

        NEW_VERSION="X.Y.Z"
        sed -i "s,^# \[in-development\] - unreleased$,# [$NEW_VERSION] - `date +%Y-%m-%d`," CHANGELOG
        sed -i "s/^    'file_format_version':'.*/    'file_format_version':'$NEW_VERSION',/" src/lisa.js
        git commit -m "maint: release $NEW_VERSION" src/lisa.js CHANGELOG
        COMMIT=$(git rev-parse HEAD | cut -c1-12)
        git tag -a -m "Added tag lisa-$NEW_VERSION for commit $COMMIT" lisa-$NEW_VERSION

3. Prepare the CHANGELOG for the next release:

        sed -i '1s;^;# \[in-development\] - unreleased\n\n;' CHANGELOG
        git commit -m "maint: prepare CHANGELOG for next release" CHANGELOG
        git push upstream main
        git push upstream lisa-$NEW_VERSION

   After the push, Gitlab will notice the `lisa-*` tag which triggers
   a build and release of LISA.
