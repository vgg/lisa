#!/usr/bin/env python3

## Copyright (C) 2022 David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
##
## Copying and distribution of this file, with or without modification,
## are permitted in any medium without royalty provided the copyright
## notice and this notice are preserved.  This file is offered as-is,
## without any warranty.

## Filter files in a LISA project.
##
## SYNOPSIS
##
##   filter-files.py [--without-regions] [--with-regions] PROJECT-FILEPATH
##
## DESCRIPTION
##
##   Filter files with specific properties from a LISA project.
##   Currently only has filters for presence/absence of regions but
##   can be easily extended.
##
## OPTIONS
##
##   --src-from FILE
##     Filter files listed on the file FILE.
##
##   --src_regex pattern
##     Filter files whose `src` matches the regular expression `pattern`.
##
##   --without-regions
##     Filter files without an annotated region.
##
##   --with-regions
##     Filter files with at least one annotated region.
##
## EXAMPLES
##
##   filter-files.py --with-regions some-project.json > project-with-regions.json


import argparse
import collections.abc
import json
import re
import sys
import typing

Region = typing.Tuple[float, float, float, float]


class File(typing.TypedDict):
    fid: str
    src: str
    regions: typing.List[Region]
    rdata: typing.List[typing.Dict]
    fdata: typing.Dict


class LisaAnnotations(typing.TypedDict):
    config: typing.Dict
    project: typing.Dict
    files: typing.List[File]
    attributes: typing.Dict


def read_lisa_annotations(
    lisa_json_fpath: str,
) -> LisaAnnotations:
    with open(lisa_json_fpath, "r") as fh:
        lisa_json = json.load(fh)
    return lisa_json


def read_src_from_file(fpath: str) -> typing.List[str]:
    with open(fpath, "r") as fh:
        return [l.rstrip() for l in fh.readlines()]

def src_regex(pattern: str, fileobj: File) -> bool:
    return bool(re.search(pattern, fileobj["src"]))


def without_regions(fileobj: File) -> bool:
    return not fileobj["regions"]


def with_regions(fileobj: File) -> bool:
    return bool(fileobj["regions"])


def main(argv: typing.List[str]) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--src-from",
        action="store",
        help="filter files whose src value is listed in the given file",
    )
    parser.add_argument(
        "--src_regex",
        action="store",
        help="filter files whose src value matches pattern",
    )
    parser.add_argument(
        "--without-regions",
        action="store_true",
        help="filter files without annotated regions",
    )
    parser.add_argument(
        "--with-regions",
        action="store_true",
        help="filter files with any annotated region",
    )
    parser.add_argument(
        "project_filepath",
        help="LISA project file path",
    )
    args = parser.parse_args(argv[1:])

    lisa_ann = read_lisa_annotations(args.project_filepath)

    predicates: typing.List[collections.abc.Callable[[File], bool]] = []
    if args.src_from:
        src_list = read_src_from_file(args.src_from)
        predicates.append(lambda f: f["src"] in src_list)
    if args.src_regex:
        predicates.append(lambda f: src_regex(args.src_regex, f))
    if args.without_regions:
        predicates.append(without_regions)
    if args.with_regions:
        predicates.append(with_regions)

    for predicate in predicates:
        lisa_ann["files"] = [f for f in lisa_ann["files"] if predicate(f)]

    print(json.dumps(lisa_ann))
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
