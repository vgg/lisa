#!/usr/bin/env python3

## Copyright (C) 2022 David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
##
## Copying and distribution of this file, with or without modification,
## are permitted in any medium without royalty provided the copyright
## notice and this notice are preserved.  This file is offered as-is,
## without any warranty.

## Remove region data from LISA project.
##
## SYNOPSIS
##
##   strip-region-data.py PROJECT-FILEPATH
##
## DESCRIPTION
##
##   Remove all region data as well as defined attributes from
##   project.


import argparse
import json
import sys
import typing


class File(typing.TypedDict):
    fid: str
    src: str
    regions: typing.List[typing.List]
    rdata: typing.List[typing.Dict]
    fdata: typing.Dict


class Attributes(typing.TypedDict):
    file: typing.Dict
    region: typing.Dict


class LisaAnnotations(typing.TypedDict):
    config: typing.Dict
    project: typing.Dict
    files: typing.List[File]
    attributes: Attributes


def read_lisa_annotations(
    lisa_json_fpath: str,
) -> LisaAnnotations:
    with open(lisa_json_fpath, "r") as fh:
        lisa_json = json.load(fh)
    return lisa_json


def main(argv: typing.List[str]) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "project_filepath",
        help="LISA project file path",
    )
    args = parser.parse_args(argv[1:])

    lisa_ann = read_lisa_annotations(args.project_filepath)

    # Empty description of region attributes in global description.
    lisa_ann["attributes"]["region"].clear()

    # Empty region annotations from all files.
    for file_obj in lisa_ann["files"]:
        file_obj["rdata"].clear()

    print(json.dumps(lisa_ann))
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
