# Export annotations for shared projects created using List Annotator (LISA)
#
# Abhishek Dutta
# 4 Oct 2020

import json
import os
import http.client
import sys
import csv
import argparse

SHARED_PROJECT_SERVER = 'meru.robots.ox.ac.uk'

def fetch_shared_project(project_id):
  conn = http.client.HTTPSConnection(SHARED_PROJECT_SERVER)
  endpoint = '/store/' + project_id
  conn.request('GET', endpoint)
  response = conn.getresponse()
  if response.status != 200:
    print('Failed to fetch annotations from http://%s%s' % (SHARED_PROJECT_SERVER, endpoint))
    print('HTTP Error: %d %s' % (response.status, response.reason))
    sys.exit(0)

  project_data = response.read().decode('utf-8')
  conn.close()
  return project_data

if __name__ == "__main__":
  parser = argparse.ArgumentParser(prog='export-annotations.py',
                                   description='Export annotations contained in a shared LISA project')
  parser.add_argument('--shared-pid-file',
                      required=True,
                      type=str,
                      help='a text file containing list of shared project id (one per line)')
  args = parser.parse_args()

  if not os.path.exists(args.shared_pid_file):
    print('Cannot open file containing shared pid list: %s' % (args.shared_pid_file))
    sys.exit(0)

  print('shared_pid,filename,x,y,width,height,class') # csv header
  with open(args.shared_pid_file) as f:
    pid_reader = csv.reader(f)
    for row in pid_reader:
      shared_pid = row[0]
      shared_project_str = fetch_shared_project(shared_pid)
      shared_project = json.loads(shared_project_str)
      for file in shared_project['files']:
        for rindex, region in enumerate(file['regions']):
          if 'class' in file['rdata'][rindex]:
            print('%s,%s,%d,%d,%d,%d,%s' % (shared_pid, file['src'], region[0], region[1], region[2], region[3], file['rdata'][rindex]['class']))
          else:
            print('%s,%s,%d,%d,%d,%d,,' % (shared_pid, file['src'], region[0], region[1], region[2], region[3]))
