#!/usr/bin/env python3

## Copyright (C) 2022 David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
##
## Copying and distribution of this file, with or without modification,
## are permitted in any medium without royalty provided the copyright
## notice and this notice are preserved.  This file is offered as-is,
## without any warranty.

## Join files sections from the same LISA project into one.
##
## SYNOPSIS
##
##   join-files.py [PROJECT-FILEPATH ...]
##
## DESCRIPTION
##
##   Given any number of LISA project files that relate to the same
##   project, joins their files sections.  This is meant to be used
##   after having split a LISA project into multiple files (see the
##   filter-files script).
##
##   The number of projects passed is any positive number, including
##   one single project.  Because the files are sorted by FID, this
##   can be used to sort the files in a single LISA project file.
##
## OPTIONS
##
##   --no-sort
##       By default, the files are sorted, numerically, by FID.  If
##       you don't want this, pass the `--no-sort` option.
##
## EXAMPLES
##
##     ## Example split and rejoin LISA project
##     filter-files.py --with-regions some-project.json > with-regions.json
##     filter-files.py --without-regions some-project.json > without-regions.json
##     join-files.py with-regions.json without-regions.json > rejoined-project.json
##
##     ## Example sort a single LISA project by FID
##     join-files.py some-project.json > sorted-project.json


import argparse
import json
import sys
import typing

Region = typing.Tuple[float, float, float, float]


class File(typing.TypedDict):
    fid: str
    src: str
    regions: typing.List[Region]
    rdata: typing.List[typing.Dict]
    fdata: typing.Dict


class LisaAnnotations(typing.TypedDict):
    config: typing.Dict
    project: typing.Dict
    files: typing.List[File]
    attributes: typing.Dict


def read_lisa_annotations(
    lisa_json_fpath: str,
) -> LisaAnnotations:
    with open(lisa_json_fpath, "r") as fh:
        lisa_json = json.load(fh)
    return lisa_json


def all_same_project(anns: typing.List[LisaAnnotations]) -> bool:
    # If the dicts relate to the same project, then "project" and
    # "attributes" should be same.  "config" might be different
    # because that includes things such as current image id.
    two_are_same = lambda p1, p2: (
        p1["project"] == p2["project"] and p1["attributes"] == p2["attributes"]
    )
    return all([two_are_same(anns[0], p) for p in anns[1:]])


def has_duplicated_fids(anns: typing.List[LisaAnnotations]) -> bool:
    # XXX: The fid is a string but in practice it seems to always be
    # an int representation.  To sort them numerically we'll convert
    # them to string so while "01" and "1" may be different strings,
    # they will be the same FID.
    fids = set()
    for i, this_ann in enumerate(anns):
        this_fids = {int(f["fid"]) for f in this_ann["files"]}
        if len(this_fids) != len(this_ann["files"]):
            raise RuntimeError("FID in project file %d are not unique" % i)
        pre_union_len = len(fids)
        fids = fids.union(this_fids)
        if len(fids) != (pre_union_len + len(this_fids)):
            return True

    return False


def main(argv: typing.List[str]) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--no-sort",
        action="store_false",
        dest="do_sort",
        help="Do not sort files after join",
    )
    parser.add_argument(
        "project_filepath",
        nargs="*",
        help="LISA project file path",
    )
    args = parser.parse_args(argv[1:])

    lisa_anns = [read_lisa_annotations(p) for p in args.project_filepath]

    if not all_same_project(lisa_anns):
        print("not the same project", file=sys.stderr)
        return 1
    if has_duplicated_fids(lisa_anns):
        print("duplicated FIDs between some projects", file=sys.stderr)
        return 1

    # The actual join
    for this_ann in lisa_anns[1:]:
        lisa_anns[0]["files"].extend(this_ann["files"])

    # The point of this script is to join files that have been
    # previously split and we want to restore the original order which
    # is why we want sort them by fid by default.  The fid is a string
    # but in practice it seems to always be an int representation so
    # convert to int to do numeric sort.
    if args.do_sort:
        lisa_anns[0]["files"] = sorted(
            lisa_anns[0]["files"],
            key=lambda f: int(f["fid"]),
        )

    print(json.dumps(lisa_anns[0]))
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
