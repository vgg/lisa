#!/usr/bin/env python3

# Pack self contained LISA HTML application
#
# Run from the project root like so:
#
#     mkdir dist
#     python3 scripts/pack_lisa_html.py > dist/lisa.html
#     python3 scripts/pack_lisa_html.py --demo > dist/lisa-demo.html

import os.path
import sys


DIST_PACK_DIR = os.path.dirname(os.path.realpath(__file__))
LISA_SRC_DIR = os.path.join(DIST_PACK_DIR, '..', 'src')


def get_src_file_contents(filename):
  full_filename = os.path.join(LISA_SRC_DIR, filename)
  with open(full_filename) as f:
    return f.read()


def pack_lisa(packing_demo):
  with open(os.path.join(LISA_SRC_DIR, 'lisa.html'), 'r') as inf:
    for line in inf:
      if '<script src="' in line:
        tok = line.split('"')
        filename = tok[1]
        print('<!-- START: Contents of file: ' + filename + '-->\n')
        print('<script>\n')
        print( get_src_file_contents(filename) )
        print('</script>\n')
        print('<!-- END: Contents of file: ' + filename + '-->\n')
      else:
        if '<link rel="stylesheet" type="text/css"' in line:
          tok = line.split('"')
          filename = tok[5]
          print('<!-- START: Contents of file: ' + filename + '-->\n')
          print('<style>\n')
          print( get_src_file_contents(filename) )
          print('</style>\n')
          print('<!-- END: Contents of file: ' + filename + '-->\n')
        else:
          if packing_demo:
            if "//__ENABLED_BY_PACK_SCRIPT__" in line:
              line = line.replace('//__ENABLED_BY_PACK_SCRIPT__', '');
          print(line)


if len(sys.argv) not in [1, 2]:
    raise Exception("wrong number of arguments")

if len(sys.argv) == 1:
    pack_demo = False
elif len(sys.argv) == 2 and sys.argv[1] == "--demo":
    pack_demo = True
else:
    raise Exception("only option argument is --demo")

pack_lisa(packing_demo=pack_demo)
